from django.db import models

# Create your models here.
class Conversion(models.Model):
    fromUnits=models.CharField(max_length=10)
    toUnits=models.CharField(max_length=10)
    value=models.DecimalField(decimal_places=10, max_digits=20)
