from django.shortcuts import render
from django.http import JsonResponse
from .models import Conversion

# Create your views here.


def init(request):
    if (Conversion.objects.all().count() == 0):
        conv = Conversion(fromUnits="lbs", toUnits="t_oz", value=14.58333)
        conv.save()
    return index(request)


def index(request):
    return render(request, 'index.html', {
    })


def convert(request):
    if not ('from' in request.GET and 'to' in request.GET and 'value' in request.GET):
        return JsonResponse({"error": "Not all parameters specified. Please include 'from', 'to', and 'value'."})

    fromUnits = request.GET['from']
    toUnits = request.GET['to']
    value = request.GET['value']
    print(fromUnits)
    print(toUnits)
    things = Conversion.objects.filter(fromUnits=fromUnits, toUnits=toUnits).all()
    if things.count() == 0:
        return JsonResponse({"error": "Unrecognized units. Please use lbs to troy ounces."})

    return JsonResponse({"units" : "t_oz", "value": (float(things.first().value) * float(value))})

