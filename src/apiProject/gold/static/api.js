//https://stackoverflow.com/questions/8489500/how-do-i-subtract-one-week-from-this-date-in-jquery#8489546
var oneWeekAgo = new Date();
oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);
//https://stackoverflow.com/questions/1531093/how-do-i-get-the-current-date-in-javascript
var stringDate = oneWeekAgo.toJSON().slice(0, 10).replace(/-/g, '-');
apiKey = "xHdBywwyyurvfpru5wS8";
var goldUrl = `https://www.quandl.com/api/v3/datasets/LBMA/GOLD.json?api_key=${apiKey}&column_index=2&start_date=${stringDate}`;
gold=0;
fetch(goldUrl).then(p => p.json()).then(data => {
    gold = data.dataset.data[0][1];
});

function convert() {
    input = document.querySelector('#input').value;
    if (input != "" && input - 1 != NaN) { //check if it can be cast to a number
        document.querySelector("#quandl").textContent = "Waiting...";
        var convertURL = "http://127.0.0.1:8000/unitconv/convert?from=lbs&to=t_oz&value=" + input;
            fetch(convertURL).then(response => response.json()).then(data => {
                console.log(data.value);
                console.log(gold);
                var text = data.value * gold;
                document.querySelector("#quandl").textContent = "$" + text.toFixed(2);
            });
    } else {
        document.querySelector("#quandl").textContent = "Please enter a numerical value";
    }
}